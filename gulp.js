const { series, parallel } = require('gulp')
const git = require('gulp-git')
const fs = require('fs')
const path = require('path')
const fs_extra = require('fs-extra')
const rimraf = require("rimraf")

const cleanFolder = function(folder, callback){
    console.log("Cleaning folder " + folder)
    if (fs.existsSync(folder)){
        try {
            rimraf.sync(folder);
            callback()
        } catch (err) {
            callback(err)
        }
    }else{
        callback()
    }
}

const removeGit = function(folder, callback){
    cleanFolder(folder + "/.git", callback)
}

function cloneProject(project, cb){
    let folder = __dirname + "/build"
    let prjFolder = folder + "/" + project
    cleanFolder(prjFolder, function(err){
        if(err)
            cb(err)
        else{
            git.clone("git@bitbucket.org:sensitprojects/" + project + ".git", {cwd: folder}, function(err){
                if(err)
                    cb(err)
                else
                    setTimeout(function(){
                        removeGit(prjFolder, cb)
                    }, 200);
            });
        }
    })
}

function cloneGis360Plus(cb){
    cloneProject("gis360plus", cb)
}
function cloneGis360MvtServer(cb){
    cloneProject("gis360plus_mvt_server", cb)
}
function cloneGis360Backend(cb){
    cloneProject("gis360plus_backend", cb)
}
function cloneGis360WebSocket(cb){
    cloneProject("gis360plus_websocket", cb)
}

function copyConfigTaskGis360Plus(cb){
    let files=["app.js","database.js","export.js","import.js","map.js"]
    for(let i=0; i<files.length; i++){
        fs_extra.copySync(
            __dirname + '/custom/config/gis360plus/' + files[i],
            __dirname + '/build/gis360plus/pwa-nodejs/config/' + files[i])
    }
    cb()
}

function copyReportTaskGis360Plus(cb){
    fs_extra.copySync(
        __dirname + '/custom/models/report/psg',
        __dirname + '/build/gis360plus/pwa-nodejs/models/report/psg'
    )
    fs_extra.removeSync(
        __dirname + '/build/gis360plus/pwa-nodejs/models/report/demo'
    )
    cb()
}

function copyConfigTaskGis360Plus_backend(cb){
    let files=["config.json"]
    for(let i=0; i<files.length; i++){
        fs_extra.copySync(
            __dirname + '/custom/config/gis360plus_backend/' + files[i],
            __dirname + '/build/gis360plus_backend/src/' + files[i])
    }
    cb()
}

function copyConfigTaskGis360Plus_mvt_server(cb){
    let files=["config.json"]
    for(let i=0; i<files.length; i++){
        fs_extra.copySync(
            __dirname + '/custom/config/gis360plus_mvt_server/' + files[i],
            __dirname + '/build/gis360plus_mvt_server/src/' + files[i])
    }
    cb()
}

function copyConfigTaskGis360Plus_websocket(cb){
    let files=["config.json"]
    for(let i=0; i<files.length; i++){
        fs_extra.copySync(
            __dirname + '/custom/config/gis360plus_websocket/' + files[i],
            __dirname + '/build/gis360plus_websocket/' + files[i])
    }
    cb()
}

module.exports.build = series(
    parallel(cloneGis360Plus, cloneGis360MvtServer, cloneGis360Backend, cloneGis360WebSocket),
    parallel(copyConfigTaskGis360Plus, copyReportTaskGis360Plus,copyConfigTaskGis360Plus_websocket,copyConfigTaskGis360Plus_mvt_server,copyConfigTaskGis360Plus_backend)
)


