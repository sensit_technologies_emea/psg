const Logos = require('./logos')

module.exports = function(data){
	return {
		pageSize: 'A4',
		//watermark: {text: 'DRAFT', opacity: 0.1, color: 'blue'},
		footer: function(currentPage, pageCount) {
			return {
				margin:[40,10],
				columns: [
					{ text: currentPage.toString() + ' of ' + pageCount, alignment: 'center' }
				]
			}
		},
		content: [
			{
				style: 'tableHeader',
				table: {
					widths: [150, '*'],
					heights: 42,
					body: [
						[
							{colSpan: 1, rowSpan: 2, image: Logos.logo,
								width: 150},
							{text: 'Session Report #' + data["session"] + '\n' + data["council"], style: 'header'}
						],
						['', {text: 'Leak #' + data["id"] + '\n' + data["datetime"], style: 'header'}],
					]
				},
				layout: {
					defaultBorder: true,
				}
			},
			{text: '\n'},
			{
				style: 'tableHeader',
				table: {
					widths: [75, 75, 75, 75, '*', 70],
					heights: 32,
					body: [
						[
							{text: 'Status:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data["status"], style: 'textValue',
							border:[false, true, false, true]
						},
							{text: 'Measure:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: parseFloat(data["measure"]).toFixed(2), style: 'textValue',
							border:[false, true, false, true]
						},
							{text: 'Battery level:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data["batterylevel"], style: 'textValue',
							border:[false, true, true, true]
						}
						],
						[
							{text: 'Speed:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: parseInt(data["speed"]), style: 'textValue',
							border:[false, true, false, true]
						},
							{text: 'Secondary measure:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: parseFloat(data["secondarymeasure"]).toFixed(2), style: 'textValue',
							border:[false, true, false, true]
						},
							{text: 'Pump flow:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data["pumpflow"], style: 'textValue',
							border:[false, true, true, true]}
						],
					]
				},
				layout: {
					defaultBorder: true,
				}
			},
			{text: 'Note', style: 'subheader'},
			{
				ol: data["notes"]
			},
			{
				image: data["mapimage"] ? data["mapimage"] : Logos.map_not_available,
				width: 520,
				alignment: 'center'
			},
			//{text: '试用版', font: 'Fangzhen', fontSize: 24, color: 'red'}
		],
		styles: {
			header: {
				fontSize: 18,
				bold: true,
				margin: [0, 0, 0, 10],
				alignment: 'center'
			},
			subheader: {
				fontSize: 16,
				bold: true,
				margin: [0, 10, 0, 5]
			},
			tableHeader: {
				margin: [0, 0, 0, 0]
			},
			textLabel:{
				bold: true,
				fontSize: 14
			},
			textValue:{
				bold: true,
				fontSize: 18,
				color: 'blue',
				alignment: 'center'
			}
		},
		defaultStyle: {
			// alignment: 'justify'
		}

	}

}
