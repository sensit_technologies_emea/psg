const Logos = require('./logos')

const getTestsTable = function(rows){
	let table = [[
		{text: 'Time', style: 'textLabel2'},
		{text: 'Type', style: 'textLabel2'},
		{text: 'Duration', style: 'textLabel2'},
		{text: 'Outcome', style: 'textLabel2'},
		{text: 'Bottle', style: 'textLabel2'},
		{text: 'Response Time', style: 'textLabel2'},
		{text: 'Openev', style: 'textLabel2'},
		{text: 'PPM', style: 'textLabel2'}
	]]
	for(let i=0; i < rows.length; i++)
		table.push([
			{text: rows[i]["time"], style: 'textValue2'},
			{text: rows[i]["type"], style: 'textValue2'},
			{text: rows[i]["duration"], style: 'textValue2'},
			{text: rows[i]["outcome"], style: 'textValue2'},
			{text: rows[i]["bottle"], style: 'textValue2'},
			{text: rows[i]["response_time"], style: 'textValue2'},
			{text: rows[i]["openev"], style: 'textValue2'},
			{text: rows[i]["ppm"], style: 'textValue2'}
		])
	return table
}

const getEventsTable = function(rows){
	let table = [[
		{text: 'Time', style: 'textLabel2'},
		{text: 'Concentration', style: 'textLabel2'},
		{text: 'Lon', style: 'textLabel2'},
		{text: 'Lat', style: 'textLabel2'},
		{text: 'Type', style: 'textLabel2'}
	]]
	for(let i=0; i < rows.length; i++)
		table.push([
			{text: rows[i]["time"], style: 'textValue2'},
			{text: rows[i]["measure"], style: 'textValue2'},
			{text: rows[i]["lon"], style: 'textValue2'},
			{text: rows[i]["lat"], style: 'textValue2'},
			{text: rows[i]["type"], style: 'textValue2'}
		])
	return table
}

module.exports = function(data){
	let template = {
		pageSize: 'A4',
		//watermark: {text: 'DRAFT', opacity: 0.1, color: 'blue'},
		footer: function(currentPage, pageCount) {
			return {
				margin:[40,10],
				columns: [
				  { text: currentPage.toString() + ' of ' + pageCount, alignment: 'center' }
				]
		  }
		},
		content: [
			{
				style: 'tableHeader',
				table: {
					widths: [140, '*'],
					heights: 50,
					body: [
						[
							{colSpan: 1, rowSpan: 2, image: Logos.logo,
							width: 140},
							{text: 'Session Report #' + data.general.session + '\n' + data.general.title, style: 'header'}
						],
						['', {text: data.general.date_start + '\n' + data.general.date_end, style: 'subheader', alignment: 'center'}],
					]
				},
				layout: {
					defaultBorder: true,
				}
			},
			{text: 'General', style: 'subheader'},
			{
				style: 'tableHeader',
				table: {
					widths: [75, 75, 75, 75, '*', 70],
					heights: 26,
					body: [
						[
							{text: 'Agency:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.agency, style: 'textValue',
								border:[false, true, false, true]
							},
							{text: 'Council:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.council, style: 'textValue',
								border:[false, true, false, true]
							},
							{text: 'Distance:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.distance + ' km', style: 'textValue',
								border:[false, true, true, true]
							}
						], [
							{text: 'Insee code:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.insee_code, style: 'textValue',
								border:[false, true, false, true]
							},
							{text: 'RSF Reference:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.rsf_reference, style: 'textValue',
								border:[false, true, false, true]
								},
							{text: 'Duration:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.duration, style: 'textValue',
								border:[false, true, true, true]}
						], [
							{text: 'Serial:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.serial, style: 'textValue',
								border:[false, true, false, true]
							},
							{text: 'Detector Reference:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.detector_reference, style: 'textValue',
								border:[false, true, false, true]
								},
							{text: 'Meteo:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.meteo, style: 'textValue',
								border:[false, true, true, true]}
						], [
							{text: 'Sensitivity:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.sensitivity, style: 'textValue',
								border:[false, true, false, true]
							},
							{text: 'Operator 1:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.operator_1, style: 'textValue',
								border:[false, true, false, true]
								},
							{text: 'Soil:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.soil, style: 'textValue',
								border:[false, true, true, true]}
						], [
							{text: 'Range monitoring:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.range_monitoring, style: 'textValue',
								border:[false, true, false, true]
							},
							{text: 'Operator 2:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.operator_2, style: 'textValue',
								border:[false, true, false, true]
								},
							{text: 'Wind:', style: 'textLabel',
								border:[true, true, false, true]
							}, {text: data.general.wind, style: 'textValue',
								border:[false, true, true, true]}
						],

					]
				},
				layout: {
					defaultBorder: true,
				}
			},
			{text: 'Tests', style: 'subheader'},
			{
				style: 'tableHeader',
				table: {
					widths: [55, 55, 55, 55, 55, 55, 55, '*'],
					heights: 20,
					body: getTestsTable(data.tests)
				}
			},
			{text: 'Events', style: 'subheader'},
			{
				style: 'tableHeader',
				table: {
					widths: [95, 95, 95, 95, '*'],
					heights: 20,
					body: getEventsTable(data.events)
				}
			}
		],
		styles: {
			header: {
				fontSize: 18,
				bold: true,
				margin: [0, 0, 0, 10],
				alignment: 'center'
			},
			subheader: {
				fontSize: 16,
				bold: true,
				margin: [0, 10, 0, 5]
			},
			tableHeader: {
				margin: [0, 0, 0, 0]
			},
			textLabel:{
				bold: true,
				fontSize: 14
			},
			textLabel2:{
				bold: true,
				fontSize: 12,
				alignment: 'center'
			},
			textValue:{
				bold: true,
				fontSize: 13,
				color: 'blue',
				alignment: 'right'
			},
			textValue2:{
				bold: false,
				fontSize: 11,
				alignment: 'center'
			}
		},
		defaultStyle: {
			// alignment: 'justify'
		}

	}

	let rows = data.leaks
	for(let i=0; i<rows.length; i++){
		let row = rows[i]
		template.content.push({
			text: 'Leak #' + row["id"],
			pageBreak: 'before',
			style: 'subheader'
		})
		template.content.push({
			text: [
				{ text: 'Date/Time: ', style: 'textLabel'},
				{ text: row["dt"], style: 'textValue' }
			]
		})
		template.content.push({
			style: 'tableHeader',
			table: {
				widths: [75, 75, 75, 75, '*', 70],
				heights: 32,
				body: [
					[
						{text: 'Status:', style: 'textLabel',
							border:[true, true, false, true]
						}, {text: row["status"], style: 'textValue',
						border:[false, true, false, true]
					},
						{text: 'Measure:', style: 'textLabel',
							border:[true, true, false, true]
						}, {text: row["measure"], style: 'textValue',
						border:[false, true, false, true]
					},
						{text: 'Battery level:', style: 'textLabel',
							border:[true, true, false, true]
						}, {text: row["batterylevel"], style: 'textValue',
						border:[false, true, true, true]
					}
					],
					[
						{text: 'Speed:', style: 'textLabel',
							border:[true, true, false, true]
						}, {text: row["speed"], style: 'textValue',
						border:[false, true, false, true]
					},
						{text: 'Secondary measure:', style: 'textLabel',
							border:[true, true, false, true]
						}, {text: row["secondarymeasure"], style: 'textValue',
						border:[false, true, false, true]
					},
						{text: 'Pump flow:', style: 'textLabel',
							border:[true, true, false, true]
						}, {text: row["pumpflow"], style: 'textValue',
						border:[false, true, true, true]}
					],
				]
			},
			layout: {
				defaultBorder: true,
			}
		})
		template.content.push({
			text: 'Note',
			style: 'subheader'
		})
		template.content.push({
			ol: [row["note_1"],row["note_2"]]
		})
		template.content.push({
			image:  row["mapimage"] ? row["mapimage"] : Logos.map_not_available,
			width: 520,
			alignment: 'center'
		})
	}
	return template
}