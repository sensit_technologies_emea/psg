config = {
    listen: 3002,
    cache: {
        enabled: false,
        folder: __dirname + "/../data/tilecache"
    },
    urlOpenStreetMap: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}",
    mode: "HTTP",
    scheduler: {
        time: "*/5 * 0-23 * * *",
        max_threads: 2
        //C:\Lavoro\Repositories\SENSIT\gis360\pwa-nodejs\data\background
    },
    acquisition: {
        session:{
            strategy: "OneDay"
        },
        receiver: {
            type: "WS2TCPIP",   //[GPS,BLE,BLE+GPS,WS2TCPIP]
            options: {
                ws_url: "ws://localhost:6789",
                tcpip_host: "46.18.29.13",
                tcpip_port: 4001,
                tcpip_machine: 51,
                chart_size: "large"
            }
        },
        pipes: true,
        baseTime: 0         //in millisecods
    },
    version: "1.7.1"
}

module.exports = config;
