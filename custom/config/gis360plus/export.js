config = {
    clientFolder: "psg",
    reportFolder: __dirname + "/../data/report/",
    tmpFolder: __dirname + "/../data/report/tmp/",
    imgFolder: __dirname + "/../public/css/images/",
}

module.exports = config ;