map = {

    mapId: {
        WITH_PIPES: 1,
        NO_PIPES: 2
    },
    groupId:{
        WITH_PIPES: 1,
        NO_PIPES: 4,
    },
    info:{
        minzoom:10,
        maxzoom:25,
        minsize:5,
        maxsize:5,
        searchpixel: 5
    },
    init:{
        lng: 19.8456573,
        lat: 52.3256764,
        zoom: 8,
        background: {
            type: "MVT" 
        }
    },
    distance: 100     // maxdistance (in meters) from the clicked point to draw the chart
}

module.exports = map;