config = {
	shape:{
		attribute_name: "DEN_PCM",
		maxZoom: 16
	},
	pipes: {
		tmp_path: __dirname + "/../data/import/pipes/tmp/",
		//EXPAINATION OF CONFIGURATION {db_field: { shape_field_name: 'column_name', default_value: value }
		db_map_from_shape_fields: {
			pipetypeid: { field_name: 'pipetypeid', default_value: 0 },
			plant: { field_name: 'plant', default_value: '' },
			administration: { field_name: 'administration', default_value: '' },
			street: { field_name: 'street', default_value: '' },
			piecescount: { field_name: 'piecescount', default_value: 0 },
			pipestatusid: { field_name: 'pipestatusid', default_value: 1  },
			lengthm: { field_name: 'lengthm', default_value: 0 }
		},
		buffer_size: 7.5 //meters
	},
	epsg: [4326,3857,2177,2180]
}

module.exports = config;