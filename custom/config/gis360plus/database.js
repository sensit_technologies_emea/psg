config = {
    user: "postgres",
    password: "postgres",
    host: "localhost",
    port: 5432,
    database: "gis360",
    attempts: 5
}

module.exports = config;